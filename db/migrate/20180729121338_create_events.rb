class CreateEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :events do |t|
      t.jsonb :payload, null: false, default: '{}'

      t.timestamps
    end
  end
end
