class AddIndexToPayloadPathOnEvents < ActiveRecord::Migration[5.2]
  def change
    execute <<-SQL
      CREATE INDEX user_prefs_newsletter_index ON events ((payload->>'issue'))
    SQL
  end
end
