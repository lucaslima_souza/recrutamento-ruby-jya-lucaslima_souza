FROM ruby:2.5.1
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
RUN mkdir /recrutamento-ruby-jya-lucaslima_souza
WORKDIR /recrutamento-ruby-jya-lucaslima_souza
COPY Gemfile /recrutamento-ruby-jya-lucaslima_souza/Gemfile
COPY Gemfile.lock /recrutamento-ruby-jya-lucaslima_souza/Gemfile.lock
RUN bundle install
COPY . /recrutamento-ruby-jya-lucaslima_souza
