class EventsController < ApplicationController
  def events
    payload = JSON.parse(request.body.read)
    @event = Event.create!(payload: payload)

    json_response @event, :created
  end

  def issues
    @issues = Event.issue(params[:number].to_i)
    json_response(@issues, :ok)
  end
end
