class Event < ApplicationRecord
  validates :payload, presence: true

  def self.issue(number)
    where('payload @> ?', { issue: { number: number } }.to_json)
   end
end
