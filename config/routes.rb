Rails.application.routes.draw do
  post 'events', to: 'events#events'
  get 'issues/:number/events', to: 'events#issues'
end
