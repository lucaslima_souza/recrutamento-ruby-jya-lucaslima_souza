require 'rails_helper'

RSpec.describe Event, type: :model do
  describe 'attributes' do
    it { is_expected.to validate_presence_of :payload }
  end

  describe 'scopes' do
    it 'select event by issue number' do
      (1..3).each do |index|
        Event.create!(payload: { action: 'open', issue: { number: index } })
      end

      expect(Event.issue(1)).not_to be_empty
      expect(Event.issue(1).first.id).to eq 1
    end
  end
end
