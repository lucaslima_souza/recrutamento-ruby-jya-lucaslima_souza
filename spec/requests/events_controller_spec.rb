require 'rails_helper'

RSpec.describe EventsController, type: :request do
  describe 'POST #events' do
    it 'create a new Event with payload' do
      headers = { 'CONTENT_TYPE' => 'application/json' }
      post_data = {
        action: 'edited',
        issue: { number: 1 }
      }.to_json

      post '/events', params: post_data, headers: headers

      expect(Event.count).to eq 1
      expect(response).to be_successful
    end
  end

  describe 'GET #issues' do
    before(:each) do
      (1..3).each do |_index|
        Event.create!(payload: { action: 'open', issue: { number: 10 } })
      end
    end

    it 'list all issues with correct number' do
      get '/issues/10/events'

      issues = JSON.parse(response.body)

      expect(issues.size).to eq 3
      expect(response).to be_successful
    end

    it 'list empty issues' do
      Event.destroy_all

      get '/issues/10/events'

      issues = JSON.parse(response.body)

      expect(issues).to be_empty
      expect(response).to be_successful
    end
  end
end
